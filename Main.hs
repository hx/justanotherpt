module Main where
import Data.Array.IO
import Control.Monad.Mersenne.Random
import System.Random.Mersenne.Pure64
import System.Random
import System.IO
import Control.Concurrent
import Data.Tuple
import Control.Monad
import Data.Word
import Data.Maybe
import Unsafe.Coerce
import qualified Data.Array.Repa as R


import Scene
import Vector
import Types
import MyScene

-- type Trace = (Double, Double, Double, Int, Word64, Int)
-- type Arr = R.Array R.U R.DIM1 Trace

-- initiateArray :: Int -> IO (Arr)
-- initiateArray size = do
--   --lst <- go 0 :: IO [Trace]
--   let lst = cycle [(0,0,0,0,0,0)]
--   return $ R.fromListUnboxed (R.Z R.:. size) lst
--   where go i = do
--                s <- randomIO :: IO Word64
--                if i == size
--                         then return []
--                         else do
--                  t <- go (i - 1)
--                  return $ (0,0,0,0,s,i):t

data PixelData = PixelData { samples :: !Int
                           , red :: !Double
                           , green :: !Double
                           , blue :: !Double
                           }

instance Show PixelData where
  show pdata = unwords [r,g,b]
    where
      r = show $ floor . (*255) $ red $ pdata
      g = show $ floor . (*255) $ green $ pdata
      b = show $ floor . (*255) $ blue $ pdata

{-# INLINE initialPixelData #-}
initialPixelData = PixelData { samples = 0
                             , red = 0.0
                             , green = 0.0
                             , blue = 0.0
                             }

{-# INLINE addToPixelData #-}
addToPixelData :: PixelData -> Radiance -> PixelData
addToPixelData pdata (Radiance r' g' b') =
  PixelData { samples = s' + 1
            , red = avrg r' r
            , green = avrg g' g
            , blue = avrg b' b
            }
  where
    avrg a' a = (a * s + a') / (s + 1)
    s' = samples pdata
    s = fromIntegral $ s'
    r = red pdata
    g = green pdata
    b = blue pdata

data Canvas = Canvas { csamples :: IOUArray Int Int
                     , cred :: IOUArray Int Double
                     , cgreen :: IOUArray Int Double
                     , cblue :: IOUArray Int Double
                     }

{-# INLINE createCanvas #-}
createCanvas :: Scene -> IO Canvas
createCanvas scene = do
  s <- newArray (0, w*h - 1) 0
  r <- newArray (0, w*h - 1) 0
  g <- newArray (0, w*h - 1) 0
  b <- newArray (0, w*h - 1) 0
  return $ Canvas s r g b
  where
        w = width $ camera $ scene
        h = heigth $ camera $ scene

{-# INLINE canvasToPixelData #-}
canvasToPixelData :: Canvas -> Int -> IO PixelData
canvasToPixelData canvas index = do
  s <- readArray (csamples canvas) index
  r <- readArray (cred canvas) index
  g <- readArray (cgreen canvas) index
  b <- readArray (cblue canvas) index
  return $ PixelData s r g b

{-# INLINE pixelDataToCanvas #-}
pixelDataToCanvas :: Canvas -> Int -> PixelData -> IO ()
pixelDataToCanvas (Canvas s r g b) i (PixelData s' r' g' b') =
  do
  writeArray s i s'
  writeArray r i r'
  writeArray g i g'
  writeArray b i b'

main :: IO ()
main = do
  canvas <- createCanvas myScene
  threads <- startThreads 8 canvas
  let filename = "test.ppm"
  mainloop canvas filename threads

  where
    mainloop canvas filename threads = do
        putStrLn "What do you want to do master?"
        putStrLn "(w)rite, (s)tats, (q)uit?"
        l <- getLine
        case l of
            "w" -> do
                writeImage canvas (w,h) filename
                showStats canvas (w*h)
                putStrLn $ "Yay! Put the output in " ++ filename ++ " dear master."
                mainloop canvas filename threads
            "s" -> do
                showStats canvas (w*h)
                mainloop canvas filename threads
            "q" -> do
                writeImage canvas (w,h) filename
                showStats canvas (w*h)
                putStrLn $ "Yay! Put the output in " ++ filename ++ " dear master."
                mapM_ killThread threads
            _   -> do
              putStrLn "Idiot. Use the defined buttons, not the undefined ones."
              mainloop canvas filename threads

    w = width $ camera $ myScene
    h = heigth $ camera $ myScene
    startThreads n canvas = sequence $ take n  $ cycle
                            [(do threadDelay 10;
                                    forkIO (traceJob myScene canvas))]

showStats :: Canvas -> Int -> IO ()
showStats canvas size = do
  paths <- go 0 size
  putStrLn $ (show $ (fromIntegral paths) / (fromIntegral size)) ++ " paths per pixel."
  where
    go :: Integer -> Int -> IO Integer
    go spp 0 = return spp
    go spp i = do
      s <- readArray (csamples canvas) (i-1)
      go (spp + (fromIntegral s)) $ i - 1

-- main = do
--   canvas <- initiateArray (myWidth*myHeigth)
--   putStrLn "Canvas done"
--   t <- repaTrace myScene canvas
--   --putStrLn $ show t
--   putStrLn "done"

writeImage :: Canvas -> (Int,Int) -> String -> IO ()
writeImage canvas (x,y) filepath = do
  putStrLn "Beginning to write now master"
  h <- openFile filepath WriteMode
  put h "P3"
  put h $ (show x) ++ " " ++ (show y)
  put h "255"
  s <- go h (x*y) 0
  hClose h
  where put h str = hPutStrLn h str
        go h mx i = if mx == i then return ()
                              else do
                     pixel <- canvasToPixelData canvas i
                     put h $ show pixel
                     go h mx (i + 1)


traceJob :: Scene -> Canvas -> IO ()
traceJob scene canvas = do
  seed <- newPureMT
  go seed 0
  where
        go s p = if p == totPixels
                 then go s 0
                 else
          do
          let (traceRes, s') = runRandom (pathTrace scene p) s
          applyTrace traceRes p
          go s' (p + 1)
        cameraWidth = width $ camera $ scene
        cameraHeigth = heigth $ camera $ scene
        totPixels = cameraHeigth * cameraWidth
        applyTrace Nothing _ = return ()
        applyTrace (Just rad) i = do
          oldRad <- canvasToPixelData canvas i
          pixelDataToCanvas canvas i $ addToPixelData oldRad rad

-- repaTrace :: Monad m => Scene -> Arr -> m Arr
-- repaTrace scene arr = R.computeP $ R.map (trace scene) arr


-- trace :: Scene
--          -> (Double, Double, Double, Int, Word64, Int)
--          -> (Double, Double, Double, Int, Word64, Int)
-- trace scene (r, g, b, samples, wseed, index) =
--   let p = (index `mod` (width $ camera $ scene)
--               ,index `quot` (heigth $ camera $ scene)) in
--   let seed = pureMT wseed in
--   let (traceRes, seed') = runRandom (pathTrace scene p) seed in
--   let wseed' = unsafeCoerce seed' in
--   if isNothing traceRes then
--     (r, b, b, samples, wseed', index)
--     else
--     let Radiance r' g' b' = fromJust (traceRes) in
--     let r'' = avrg r' r in
--     let g'' = avrg g' g in
--     let b'' = avrg b' b in
--     (r'', g'', b'',samples + 1, wseed', index)

--         where avrg a' a = (a * s + a') / (s + 1)
--               s = fromIntegral samples

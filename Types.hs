module Types where

import Control.Monad.Mersenne.Random

data Color = Color !Double !Double !Double
data Radiance = Radiance !Double !Double !Double

unitRadiance :: Radiance
unitRadiance = Radiance 1.0 1.0 1.0

applyColor :: Color -> Radiance -> Radiance
applyColor (Color r g b) (Radiance x y z) = Radiance (r*x)(g*y)(b*z)

type IOR = Double

data Material = Lambertian !Color
              | Emitter !Color
              | Mirror !Color
              | Strange !Color
              | Refractive !Color !IOR

isLambertian (Lambertian _) = True
isLambertian _ = False

isMirror (Mirror _) = True
isMirror _ = False

isRefractive (Refractive _ _) = True
isRefractive _ = False

isStrange (Strange _) = True
isStrange _ = False

{-# INLINE isEmitter #-}
isEmitter :: Material -> Rand Bool
isEmitter (Emitter _) = return True
isEmitter _ = return False

applyMaterial :: Material -> Radiance -> Rand Radiance
applyMaterial (Lambertian col) rad = return $ applyColor col rad
applyMaterial (Emitter col) rad = return $ applyColor col rad
applyMaterial (Mirror col) rad = return $ applyColor col rad
applyMaterial (Refractive col _) rad = return $ applyColor col rad
applyMaterial (Strange col) rad = return $ applyColor col rad
--applyMaterial _ _ = error "Material does not exist"

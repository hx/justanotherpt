module Scene where -- (Scene, Sphere, Objects, Object, Camera, Intersection, pathTrace)

import Control.Monad.Mersenne.Random
import Data.Maybe
import Data.List
import Debug.Trace

import Vector
import Types

data Scene = Scene { objects :: !Objects
                   , camera :: !Camera
                   , deathRatio :: !Double
             }

data Sphere = Sphere { origin :: !Vector
                     , radius :: !Double }

data Plane = Plane { pnormal :: !Vector
                   , ppos :: !Vector }

data Objects = Objects ![Object]

data Shape = SphereShape !Sphere
           | PlaneShape !Plane

data Object = Object !Material !Shape

data Camera = Camera { position :: !Vector
                     , focalLength :: !Double
                     , width :: !Int
                     , heigth :: !Int
                     , apature :: !Double
                     , focalDistance :: !Double
                     }

data Intersection = Intersection { intersectionPoint :: !Vector
                                 , intersectionNormal :: !Vector
                                 , incomming :: !Vector
                                 , material :: Material
                                 , distance :: !Double
                                 }

instance Eq Intersection where
  a == b = (distance a) == (distance b)

instance Ord Intersection where
  compare a b = compare (distance a) (distance b)

epsilon :: Double
epsilon = 0.00001

{-# INLINE applyWorldColor #-}
applyWorldColor :: Scene -> Radiance -> Ray -> Rand (Maybe Radiance)
applyWorldColor scene radiance ray = return $ Just $
                                     applyColor worldCol radiance
  where
    worldCol = Color 1 0 1

{-# INLINE pathTrace #-}
pathTrace :: Scene -> Int -> Rand (Maybe Radiance)
pathTrace scene pixel = do
  ray <- rayForPixel scene pixel
  traceRay scene unitRadiance ray

getDoF :: Double -> Rand (Double,Double)
getDoF d = do
  x <- getDouble
  y <- getDouble
  let x' = x * 2 - 1
  let y' = y * 2 - 1
  if x'*x' + y'*y' <= 1
    then return ((p x') * d, (p y') * d)
    else getDoF d
  where p a = (- 3 * (a*0.9)^8 + (a*0.9)^2 + 0.68) * a


{-# INLINE rayForPixel #-}
rayForPixel :: Scene -> Int -> Rand Ray
rayForPixel scene p = do
  xoffset <- getDouble
  yoffset <- getDouble
  let x'' = x' + xoffset * pixelsize
  let y'' = y' + yoffset * pixelsize
  let dirVec = normalize $ Vec (-x'') fov (-y'')
  (xdof, zdof) <- getDoF (fov/ap)
  let o = (Vec (cx + xdof) cy (cz + zdof))
  let dirVec = normalize $ (Vec (-x'' * fs) focdist (-y'' * fs)) |-| o
  return $ Ray o dirVec
  where x = p `mod` (width $ camera scene)
        y = p `quot` (width $ camera scene)
        origin = position $ camera scene
        (Vec cx cy cz) = origin
        fov = focalLength $ camera scene
        w = fromIntegral $ width $ camera scene
        h = fromIntegral $ heigth $ camera scene
        scale = max w h
        x' = fromIntegral x / scale - (w / scale) / 2
        y' = fromIntegral y / scale - (w / scale) / 2
        pixelsize = 1.0 / scale
        ap = apature $ camera scene
        focdist = focalDistance $ camera scene
        fs = focdist / fov

{-# INLINE traceRay #-}
traceRay :: Scene -> Radiance -> Ray -> Rand (Maybe Radiance)
traceRay scene radiance ray = do
  russian <- getDouble
  if russian < deathRatio scene then return Nothing else
    let maybeClosest = intersectObjects (objects scene) ray in
    let closestIntersection = fromJust maybeClosest in
    if isNothing maybeClosest
        then applyWorldColor scene radiance ray
        else do
      radiance' <- applyObjectColor closestIntersection radiance
      emitter <- isEmitter $ material closestIntersection
      if emitter then return $ Just radiance' else
        do
          ray' <- newRay closestIntersection
          traceRay scene radiance' ray'

{-# INLINE applyObjectColor #-}
applyObjectColor :: Intersection -> Radiance -> Rand Radiance
applyObjectColor intersection radiance =
  applyMaterial (material intersection) radiance

{-# INLINE intersectObjects #-}
intersectObjects :: Objects -> Ray -> Maybe Intersection
intersectObjects (Objects (objs)) ray = go Nothing objs
  where go Nothing (o:os) = let i = intersectObject ray o in
          go i os
        go a (o:os) = let i = intersectObject ray o in
          if isNothing i then go a os else
            go (Just ( min (fromJust i) (fromJust a))) os
        go a _ = a


{-# INLINE newRay #-}
newRay :: Intersection -> Rand Ray
newRay intersection
  | isLambertian m = lambertianRay intersection
  | isMirror m = return $ reflectedRay intersection
  | isRefractive m = reflectedOrRefractedRay intersection
  | isStrange m = return $ strangeRay intersection
    where m = material intersection

strangeRay :: Intersection -> Ray
strangeRay intersection = Ray o n
      where o = intersectionPoint intersection
            n = intersectionNormal intersection

lambertianRay :: Intersection -> Rand Ray
lambertianRay intersection = do
    rvec <- randomDirection
    return $ Ray o (normalize $ n' |+| rvec)
      where o = intersectionPoint intersection
            n = intersectionNormal intersection
            n' = if fromInside then scale n (-1) else n
            dir = incomming intersection
            fromInside = n |.| dir > 0

reflectedRay :: Intersection -> Ray
reflectedRay intersection =
    let a = scale n (2 * (n |.| v)) in
    let d =  normalize $ scale (a |-| v) (-1) in
    Ray o d
      where o = intersectionPoint intersection
            n = intersectionNormal intersection
            v = incomming intersection

reflectedOrRefractedRay :: Intersection -> Rand Ray
reflectedOrRefractedRay intersection = do
  a <- getDouble
  case (reflectance intersection) > a of
    True -> return $ reflectedRay intersection
    False -> return $ refractedRay intersection

refractedRay :: Intersection -> Ray
refractedRay intersection = if sinT2 > 1
                            then reflectedRay intersection
                            else Ray o v
  where
    cosI = 0 - norm' |.| inc
    sinT2 = r * r * (1-cosI*cosI)
    cosT = sqrt (1 - sinT2)

    inc' = scale inc r
    norm' = if isInside then scale norm (-1) else norm
    norm'' = scale norm' $ r * cosI - cosT

    v = inc' |+| norm''

    inc = incomming $ intersection
    norm = intersectionNormal $ intersection
    o = intersectionPoint $ intersection
    r = case not isInside of
      True -> 1/ior
      False -> ior
    isInside = norm |.| inc > 0
    (Refractive _ ior) = material $ intersection

reflectance :: Intersection -> Double
reflectance intersection =
  r0 + (1 - r0) * x * x * x * x * x
  where cosTheta = 0 - (norm' |.| dir)
        n1 = if fromInside then 1 else ior
        n2 = if fromInside then ior else 1
        n = n1 / n2
        sin2Theta = n * n * (1 - cosTheta * cosTheta)
        cosThetaFromInside = sqrt (1 - sin2Theta)
        r0 = (\a -> a*a) $ (n1-n2)/(n1 + n2)
        x = 1 - (if fromInside then cosThetaFromInside else cosTheta)

        norm = intersectionNormal $ intersection
        norm' = if fromInside then scale norm (-1) else norm
        dir = incomming $ intersection
        (Refractive _ ior) = material $ intersection
        fromInside = norm |.| dir > 0

{-# INLINE intersectObject #-}
intersectObject :: Ray -> Object -> Maybe Intersection
intersectObject ray (Object mat (SphereShape sphere)) = do
  i <- (intersectSphere ray sphere)
  return $ i {material = mat}
intersectObject ray (Object mat (PlaneShape plane)) = do
  i <- intersectPlane ray plane
  return $ i {material = mat}

{-# INLINE intersectSphere #-}
intersectSphere :: Ray -> Sphere -> Maybe Intersection
intersectSphere (Ray rorigin dir) (Sphere sorigin radius) =
  if tca < 0 || thcsquared < 0 || tb < epsilon
  then Nothing
  else
    Just Intersection { intersectionPoint = intPoint
                        , intersectionNormal = normalize (intPoint |-| sorigin)
                        , incomming = dir
                        , material = undefined
                        , distance = t
           }

  where relPos = sorigin |-| rorigin
        tca = relPos |.| dir
        dsquared = relPos |.| relPos - tca * tca
        thcsquared = radius * radius - dsquared
        thc = sqrt thcsquared
        ta = min (tca - thc) (tca + thc)
        tb = max (tca - thc) (tca + thc)
        intPoint = scale dir t |+| rorigin
        t = if thc == 0
            then thc
            else if
              ta > epsilon
              then ta - epsilon
              else tb + epsilon

{-# INLINE intersectPlane #-}
intersectPlane :: Ray -> Plane -> Maybe Intersection
intersectPlane (Ray p0 v) (Plane pnorm pos) =
  if b == 0.0 || t <= 0 then
    Nothing
  else
    Just Intersection { intersectionPoint = intPoint
                        , intersectionNormal = intNorm
                        , incomming = v
                        , material = undefined
                        , distance = t'
                        }
  where
    d = pnorm |.| pos
    a = - (p0 |.| pnorm) - d
    b = v |.| pnorm
    t = a / b
    t' = t - epsilon
    intPoint = p0 |+| (scale v t')
    intNorm = if b > 0 then scale pnorm (-1) else pnorm

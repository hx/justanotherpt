module Vector where

import Control.Monad.Mersenne.Random

data Vector = Vec !Double !Double !Double
              deriving (Eq,Show)

data Point = Point !Double !Double !Double

{-# INLINE toPoint #-}
toPoint :: Vector -> Point
toPoint (Vec a b c) = Point a b c

{-# INLINE toVector #-}
toVector :: Point -> Vector
toVector (Point a b c) = Vec a b c

{-# INLINE (|+|) #-}
(|+|) :: Vector -> Vector -> Vector
(Vec a b c) |+| (Vec x y z) = Vec (a+x) (b+y) (c+z)

{-# INLINE (|-|) #-}
(|-|) :: Vector -> Vector -> Vector
(Vec a b c) |-| (Vec x y z) = Vec (a-x) (b-y) (c-z)

{-# INLINE (|.|) #-}
(|.|) :: Vector -> Vector -> Double
(Vec a b c) |.| (Vec x y z) = (a*x)+(b*y)+(c*z)

{-# INLINE (|*|) #-}
(|*|) :: Vector -> Vector -> Vector
(Vec a b c) |*| (Vec x y z) = Vec (b*z - c*y) (c*x - a*z) (a*y - b*x)

{-# INLINE scale #-}
scale :: Vector -> Double -> Vector
scale (Vec a b c) k = Vec (a * k)(b * k)(c * k)

{-# INLINE vectorLength #-}
vectorLength :: Vector -> Double
vectorLength (Vec a b c) = sqrt $ a*a + b*b + c*c

{-# INLINE normalize #-}
normalize :: Vector -> Vector
normalize v = scale v $ 1/(vectorLength v)

{-# INLINE randomDirection #-}
randomDirection :: Rand (Vector)
randomDirection = do
  x <- getDouble
  y <- getDouble
  z <- getDouble
  let x' = 2 * x - 1
  let y' = 2 * y - 1
  let z' = 2 * z - 1
  if x'*x' + y'*y' + z'*z' <= 1 then return $ normalize $ Vec x' y' z'
                         else randomDirection

data Ray = Ray { origin :: Vector
               , dir :: Vector
               }

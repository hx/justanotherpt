{-# LANGUAGE GeneralizedNewtypeDeriving #-}
module TraceM where

import Control.Monad
import Control.Monad.State
import Control.Monad.Reader
import Control.Monad.Mersenne.Random
import Control.Monad.RWS.Strict

import Types (Scene, Radiance, Color, applyColor)
import Vector

type TraceState = StateT Radiance
type TraceReader = ReaderT Scene
type TraceRandom = Rand
type TraceMaybe = MaybeT



-- newtype MaybeT m a = MaybeT {
--   runMaybeT :: m (Maybe a)
--   }

-- instance (Monad m) => Monad (MaybeT m) where
--   return a = MaybeT $ return (Just a)
--   x >>= f = MaybeT $ runMaybeT x >>= maybe (return Nothing) (runMaybeT . f)
--   fail _ = MaybeT $ return Nothing

-- newtype TraceM a = TraceM {
--   runTraceM :: RWST Scene String Radiance (MaybeT Rand) a
--   } deriving (Monad)

-- instance MonadRWS Scene String Radiance TraceM where



-- scene :: TraceM a
-- scene = ask

scene :: TraceM Scene

apply :: Color -> TraceM ()

random :: TraceM Double


-- Random $ Maybe Radiance

{- List of needed monadic functions in the TraceM monad listed below:
applyColor



-}

main :: IO()
main = putStrLn "abc"

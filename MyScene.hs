module MyScene (myScene) where
import Scene
import Vector
import Types

{-# INLINE myCamera #-}
myCamera :: Camera
myCamera = Camera { position = Vec 0 0 0
         , focalLength = 1.0
         , width = myWidth
         , heigth = myHeigth
         , apature = 8
         , focalDistance = 4.3
         }

{-# INLINE myWidth #-}
myWidth = 256
{-# INLINE myHeigth #-}
myHeigth = 256

{-# INLINE myScene #-}
myScene :: Scene
myScene = Scene { objects = myCornellScene
                , camera = myCamera
                , deathRatio = 0.1
                }

myCornellScene = Objects [roof, floor, left, right, back, behind
                         , sphere1
                         , sphere2
                         , sphere3
                         , inside]
  where
    light = Object pureWhite $ SphereShape $ Sphere (Vec 0 4.5 1.5) 0.5
    roof = Object white $ SphereShape $ Sphere (Vec 0 4.5 (10000 + 1.5)) 10000
    floor = Object white $ SphereShape $ Sphere (Vec 0 4.5 (-10000 - 1.5)) 10000
    right = Object pureWhite $ SphereShape $ Sphere (Vec (-10000 - 1.5) 4.5 0) 10000
    left = Object blue $ SphereShape $ Sphere (Vec (10000 + 1.5) 4.5 0) 10000
    back = Object green $ SphereShape $ Sphere (Vec 0 10007.5 0) 10000
    behind = Object white $ SphereShape $ Sphere (Vec 0 (-9999.9) 0) 10000
    inside = Object mirror $ SphereShape $ Sphere (Vec 0.1 0.1 0.1) 6

sphere1 = Object glass $ SphereShape $ Sphere (Vec (-0.5) 4 (-1)) 0.5
sphere2 = Object glass $ SphereShape $ Sphere (Vec (0.5) 4.5 (-1)) 0.5
sphere3 = Object blue $ SphereShape $ Sphere (Vec (0.5) 4.5 (-1)) 0.45
sphere4 = Object green $ SphereShape $ Sphere (Vec (0.0) 4.0 0) 0.1


red = Lambertian $ Color 0.75 0.25 0.25
green = Lambertian $ Color 0.25 0.75 0.25
blue = Lambertian $ Color 0.25 0.25 0.75
white = Lambertian $ Color 0.75 0.75 0.75

strange = Strange $ Color 0.75 0.75 0.75

mirror = Mirror $ Color 0.9 0.9 0.9

pureWhite = Emitter $ Color 1 1 1

glass = Refractive (Color 1 1 1) 1.5
